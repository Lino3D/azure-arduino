#include <b64.h>
#include <HttpClient.h>

#include <ArduinoJson.h>

#include <LWiFi.h>
#include <LWiFiClient.h>
#include <LWiFiServer.h>
#include <LWiFiUdp.h>
#include "GpsHandler.h"
#include "MobileServiceClient.h"

#include <LGPS.h>
#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>


#define Drv LFlash
#define WIFI_PASS "Piesek44"
#define WIFI_AP "Pomarancza"



char buff[256];
gpsSentenceInfoStruct g_info;
int wifiStatus;
MobileServiceClient* azureServiceClient;
GpsHandler* gpsHandler;

void writeLog(String message, char* fileName)
{
	LFile dataFile = Drv.open(fileName, FILE_WRITE);
	if (dataFile) {
		dataFile.println(message);
		dataFile.close();
		// print to the serial port too:
		Serial.println(message);
	}
	// if the file isn't open, pop up an error:
	else {
		Serial.println("error opening file");
	}
}
int connectToWifi()
{
	while (LWiFi.status() != LWIFI_STATUS_CONNECTED)
	{
		if (LWiFi.connectWPA(WIFI_AP, WIFI_PASS))
		{
			Serial.println("Connected to wifi!");
			return 1;
		}
		else
		{
			Serial.println("Error - failed to connect to WiFi");
			return 0;
		}
	}
}


void setup() {
	Serial.begin(9600);
	Serial.print("Initializing SD card...");
	// make sure that the default chip select pin is set to
	// output, even if you don't use it:
	pinMode(10, OUTPUT);
	wifiStatus = connectToWifi();
	if (wifiStatus == 1)
	{

		// see if the card is present and can be initialized:
		Drv.begin();
		Serial.println("Storage initialized.");
		delay(3000);
		gpsHandler = new GpsHandler();
		LGPS.powerOn();
		Serial.println("LGPS Power on, and waiting ...");
		//delay(5000);
		azureServiceClient = new MobileServiceClient("azuredogtracker.azurewebsites.net");
	}
}

void loop() {
	//getGPSData(g_info);
	delay(3000);
	if (wifiStatus == 1)
	{
		JsonObject* gpsData = gpsHandler->GetGpsData();		
		if (gpsData->containsKey("Misc"))
		{
			char buffer[200];
			int length = gpsData->printTo(buffer, sizeof(buffer));
			Serial.println("Wchodzi tutaj");
			Serial.println( );
			//azureServiceClient->restOperation(
			//"/tables/dogsampledata",
			//HTTP_METHOD_POST,
			//gpsData
			//);
			//delay(20000);
		}
		else
			delay(3000);
	}
	else
	{
		Serial.println("Wifi Off");
	}
}
//char* logName = (char*)calloc(40, sizeof(char));
//strcpy(logName, time);
//strcpy(logName, "gps_Log.txt");
//String logFile(logName);
//writeLog(finalMessage, logName);
//free(logName);
//return true;