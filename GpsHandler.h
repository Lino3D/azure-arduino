#include <ArduinoJson.h>
#include <LGPS.h>

#ifndef GpsHandler_h
#define GpsHandler_h

class GpsHandler
{
public:
	GpsHandler();
	JsonObject* GetGpsData();
	JsonObject* CreateJson();
	const char* nextElement(const char* src, char* buf);

private:
	gpsSentenceInfoStruct g_info;

	bool gps_fix;
	const char* lastLattitude;
	const char* lastLongitude;
	//JsonObject& gpsData;
};
#endif