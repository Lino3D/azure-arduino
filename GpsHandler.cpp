#include "GpsHandler.h"
#include <ArduinoJson.h>
#include <LGPS.h>

GpsHandler::GpsHandler()
{
}

JsonObject* GpsHandler::GetGpsData()
{
	LGPS.getData(&g_info);
	return CreateJson();;
}

JsonObject* GpsHandler::CreateJson()
{
	char* dataString = (char*)g_info.GPGGA;
	char SMScontent[160];
	char latitude[20];
	char lat_direction[1];
	char longitude[20];
	char lon_direction[1];
	char buf[20];
	char time[30];
	const char* element = dataString;
	element = nextElement(element, 0); // GGA overall info
	element = nextElement(element, time); // Time taken
	element = nextElement(element, latitude); // Latitude
	element = nextElement(element, lat_direction); // N or S?
	element = nextElement(element, longitude); // Longitude
	element = nextElement(element, lon_direction); // E or W?
	element = nextElement(element, buf); // fix quality

	StaticJsonBuffer<4096> jsonBuffer;
	JsonObject  &gpsData = jsonBuffer.createObject();
		//if there's a GPS fix
	if (buf[0] != '0')
	{
		if (lastLattitude == latitude && lastLongitude == longitude)
		{
			Serial.println("Nic si� nie zmieni�o!");
			//delay(500000);
		}
		element = nextElement(element, buf); // number of satellites
		delay(2000);
		//Serial.print("GPS is fixed:");
		//Serial.println(atoi(buf));
		//Serial.println(" satellite(s) found!");
		char lonData[21];
		char latData[21];
		strcpy(lonData, longitude);
		strcat(lonData, lon_direction);
		strcpy(latData, latitude);
		strcat(latData, lat_direction);

		Serial.println(lonData);
		Serial.println(latData);
		gpsData["Longitude"] = lonData;
		gpsData["Latitude"] = latData;
		gpsData["Misc"] = buf;

		lastLattitude = latData;
		lastLongitude = lonData;

	}
	else
	{
		delay(3000);
		Serial.println(dataString);
		Serial.println("GPS is not fixed yet.");
	}
	return &gpsData;
}

const char * GpsHandler::nextElement(const char * src, char * buf)
{
	int i = 0;
	while (src[i] != 0 && src[i] != ',')
		i++;
	if (buf)
	{
		strncpy(buf, src, i);
		buf[i] = 0;
	}
	if (src[i])
		i++;
	return src + i;
}